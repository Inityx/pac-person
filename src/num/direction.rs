use core::{
    iter,
    ops::Neg,
    slice,
};
use num_traits::{
    identities,
};
use super::{BaseNumeric, Coord};

const DIRECTION_PRIORITY: [Direction; 4] = [
    Direction::Up,
    Direction::Left,
    Direction::Down,
    Direction::Right,
];

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Direction { Up, Left, Down, Right }

impl Direction {
    pub fn invert(&mut self) {
        *self = -*self;
    }

    pub fn as_coord<N: BaseNumeric>(self) -> Coord<N> {
        use Direction::*;
        use identities::{zero, one};

        match self {
            Left  => Coord::at(-one::<N>(), zero::<N>()),
            Right => Coord::at( one::<N>(), zero::<N>()),
            Up    => Coord::at(zero::<N>(),  one::<N>()),
            Down  => Coord::at(zero::<N>(), -one::<N>()),
        }
    }
}

impl Neg for Direction {
    type Output = Self;
    fn neg(self) -> Self::Output {
        use Direction::*;
        match self {
            Left => Right,
            Right => Left,
            Up => Down,
            Down => Up,
        }
    }
}

pub struct DirectionPriority(iter::Cloned<slice::Iter<'static, Direction>>);

impl DirectionPriority {
    pub fn new() -> Self {
        Self(DIRECTION_PRIORITY.iter().cloned())
    }
}

impl Iterator for DirectionPriority {
    type Item = Direction;
    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}

impl DoubleEndedIterator for DirectionPriority {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.0.next_back()
    }
}
