use core::{
    iter::{FusedIterator, Rev},
    num::NonZeroU8,
    ops::Range,
};

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Tick { Tock, Tick(NonZeroU8) }

impl Tick {
    fn from_state(n: u8) -> Self {
        match n {
            0 => Tick::Tock,
            n => Tick::Tick(unsafe { NonZeroU8::new_unchecked(n) }),
        }
    }

    pub fn is_tock(&self) -> bool {
        *self == Self::Tock
    }
}

/// Vends cycles of N `Tick`s, with 1 `Tick::Tock` and N-1 `Tick::Tick`s counting up from 1
pub struct Periodic {
    state: u8,
    end: u8,
}

impl Periodic {
    pub fn once_every(n: u8) -> Self {
        if n == 0 { panic!("Tried to construct a Periodic with count 0"); }

        let end = n - 1;
        Self {
            state: end,
            end,
        }
    }

    pub fn get(&self) -> Tick {
        Tick::from_state(self.state)
    }

    pub fn next(&mut self) -> Tick {
        self.cyclic_increment();
        self.get()
    }

    fn cyclic_increment(&mut self) {
        // Avoid modulo because AVR has no divmod hardware
        self.state = if self.state == self.end { 0 } else { self.state + 1 };
    }
}

/// Counts down from N-1 to 0, then fusing to None
#[derive(Debug)]
pub struct CountDown(Rev<Range<u8>>);

impl CountDown {
    pub fn from(n: u8) -> Self {
        Self((0..n).rev())
    }

    pub fn immediate() -> Self {
        Self::from(0)
    }
}

impl Iterator for CountDown {
    type Item = u8;
    fn next(&mut self) -> Option<Self::Item> {
        self.0.next()
    }
}

impl FusedIterator for CountDown {}

/// Returns alternating series of `true` and `false`, then fusing to the inverse of the last value
pub struct BoolSequence<'a> {
    count_down: Option<CountDown>,
    value: bool,
    counts: core::slice::Iter<'a, u8>,
}

impl<'a> BoolSequence<'a> {
    pub fn starting_with(value: bool, counts: &'a [u8]) -> Self {
        let mut counts = counts.iter();
        let count_down = counts.next().copied().map(CountDown::from);

        Self { count_down, value, counts }
    }

    pub fn next(&mut self) -> bool {
        loop {
            let count_down = match &mut self.count_down {
                Some(count_down) => count_down,
                None => break self.value,
            };

            if count_down.next().is_some() {
                break self.value;
            }

            self.count_down = self.counts.next().copied().map(CountDown::from);
            self.value = !self.value;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn periodic_start_tock() {
        let mut p = Periodic::once_every(3);
        assert_eq!(Tick::Tock, p.next());
    }

    #[test]
    #[should_panic]
    fn periodic_zero_panics() {
        Periodic::once_every(0);
    }

    #[test]
    fn periodic_n() {
        let mut p = Periodic::once_every(3);
        assert_eq!(Tick::Tock, p.next());
        assert_eq!(Tick::Tick(unsafe { NonZeroU8::new_unchecked(1) }), p.next());
        assert_eq!(Tick::Tick(unsafe { NonZeroU8::new_unchecked(2) }), p.next());

        assert_eq!(Tick::Tock, p.next());
        assert_eq!(Tick::Tick(unsafe { NonZeroU8::new_unchecked(1) }), p.next());
        assert_eq!(Tick::Tick(unsafe { NonZeroU8::new_unchecked(2) }), p.next());
    }

    #[test]
    fn countdown_immediate() {
        let mut cd = CountDown::immediate();
        assert_eq!(None, cd.next());
    }

    #[test]
    fn countdown_n() {
        let mut cd = CountDown::from(3);
        assert_eq!(Some(2), cd.next());
        assert_eq!(Some(1), cd.next());
        assert_eq!(Some(0), cd.next());
        assert_eq!(None, cd.next());
    }

    #[test]
    fn countdown_fused() {
        let mut cd = CountDown::from(3);

        assert_eq!(Some(2), cd.next());
        assert_eq!(Some(1), cd.next());
        assert_eq!(Some(0), cd.next());
        assert!(
            core::iter::repeat_with(|| cd.next())
                .take(500)
                .all(|val| val.is_none())
        );
    }

    #[test]
    fn sequence_empty() {
        let timings = [];
        let mut seq = BoolSequence::starting_with(false, &timings);

        assert!(
            core::iter::repeat_with(|| seq.next())
                .take(500)
                .all(|val| val == false)
        );
    }

    #[test]
    fn sequence_nm() {
        let timings = [2, 3, 2];
        let mut seq = BoolSequence::starting_with(true, &timings);

        assert_eq!(true, seq.next());
        assert_eq!(true, seq.next());
        assert_eq!(false, seq.next());
        assert_eq!(false, seq.next());
        assert_eq!(false, seq.next());
        assert_eq!(true, seq.next());
        assert_eq!(true, seq.next());
        assert!(
            core::iter::repeat_with(|| seq.next())
                .take(500)
                .all(|val| val == false)
        );
    }
    #[test]
    fn sequence_zero() {
        let timings = [2, 0, 2];
        let mut seq = BoolSequence::starting_with(true, &timings);

        assert_eq!(true, seq.next());
        assert_eq!(true, seq.next());
        assert_eq!(true, seq.next());
        assert_eq!(true, seq.next());
        assert!(
            core::iter::repeat_with(|| seq.next())
                .take(500)
                .all(|val| val == false)
        );
    }
}