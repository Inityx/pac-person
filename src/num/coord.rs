use core::{
    fmt,
    ops::{Add, Sub, Mul, Neg},
};
use num_traits::{
    cast::AsPrimitive,
};
use super::{BaseNumeric, Direction};

#[derive(PartialEq, Eq, Default, Clone, Copy)]
pub struct Coord<N> { pub x: N, pub y: N }

impl<N: fmt::Debug> fmt::Debug for Coord<N> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "({:?}, {:?})", self.x, self.y)
    }
}

impl<N> Coord<N> {
    pub const fn at(x: N, y: N) -> Self {
        Coord { x, y }
    }

    pub fn dist_sq_to(self, other: Self) -> usize
    where
        N: Sub,
        <N as Sub>::Output: AsPrimitive<isize>,
    {
        (self - other).magnitude_sq()
    }

    pub fn magnitude_sq(&self) -> usize
    where N: AsPrimitive<isize>
    {
        let a_squared = self.x.as_() * self.x.as_();
        let b_squared = self.y.as_() * self.y.as_();
        (a_squared + b_squared) as usize
    }
}

impl<N: fmt::Display> fmt::Display for Coord<N> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "({}, {})", self.x, self.y)
    }
}

impl<N: Add<M>, M> Add<Coord<M>> for Coord<N> {
    type Output = Coord<<N as Add<M>>::Output>;
    fn add(self, rhs: Coord<M>) -> Self::Output {
        Coord {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl<M, N: Sub<M>> Sub<Coord<M>> for Coord<N> {
    type Output = Coord<<N as Sub<M>>::Output>;
    fn sub(self, rhs: Coord<M>) -> Self::Output {
        Coord {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl<N: Add + BaseNumeric> Add<Direction> for Coord<N> {
    type Output = Coord<<N as Add>::Output>;
    fn add(self, rhs: Direction) -> Self::Output {
        self + rhs.as_coord::<N>()
    }
}

impl<N: Sub + BaseNumeric> Sub<Direction> for Coord<N> {
    type Output = Coord<<N as Sub>::Output>;
    fn sub(self, rhs: Direction) -> Self::Output {
        self - rhs.as_coord::<N>()
    }
}

impl<M: Clone, N: Mul<M>> Mul<M> for Coord<N> {
    type Output = Coord<<N as Mul<M>>::Output>;
    fn mul(self, rhs: M) -> Self::Output {
        Coord {
            x: self.x * rhs.clone(),
            y: self.y * rhs,
        }
    }
}

impl<N: Neg> Neg for Coord<N> {
    type Output = Coord<<N as Neg>::Output>;
    fn neg(self) -> Self::Output {
        Coord {
            x: -self.x,
            y: -self.y,
        }
    }
}