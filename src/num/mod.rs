use core::ops::Neg;
use num_traits::identities;

mod direction;
mod coord;
mod periodic;

pub trait BaseNumeric = identities::Zero + identities::One + Neg<Output=Self>;

pub type Score = u16;

pub use coord::Coord;
pub use direction::{Direction, DirectionPriority};
pub use periodic::{Periodic, Tick, CountDown, BoolSequence};

pub type TileIndex = i8;
pub type TileCoord = Coord<TileIndex>;

impl TileCoord {
    pub fn as_px_coord(self) -> PxCoord {
        Coord::at(self.x as PxIndex, self.y as PxIndex) * crate::game::TILE_PX as PxIndex
    }
}

pub type PxIndex = isize;
pub type PxCoord = Coord<PxIndex>;