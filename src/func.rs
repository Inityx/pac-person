pub struct Not<F>(pub F);

impl<F, T> FnOnce<(T,)> for Not<F> where F: FnOnce(T) -> bool {
    type Output = bool;
    extern "rust-call" fn call_once(self, (t,): (T,)) -> Self::Output {
        !self.0(t)
    }
}

impl<F, T> FnMut<(T,)> for Not<F> where F: FnMut(T) -> bool {
    extern "rust-call" fn call_mut(&mut self, (t,): (T,)) -> Self::Output {
        !self.0(t)
    }
}

impl<F, T> Fn<(T,)> for Not<F> where F: Fn(T) -> bool {
    extern "rust-call" fn call(&self, (t,): (T,)) -> Self::Output {
        !self.0(t)
    }
}
