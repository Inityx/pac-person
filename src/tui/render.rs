use std::io::{self, prelude::*};
use termion::{color, cursor, clear, raw::{IntoRawMode, RawTerminal}};
use pacperson::{
    game::{
        Game,
        classic_data,
        board::{Board, TileKind, PacDot},
        entity::{player::Player, ghost::{Ghost, Appearance as GhostAppearance, Behavior as GhostBehavior}},
    },
    num::{Coord, TileCoord},
};

fn goto_coord(Coord { x, y }: TileCoord) -> cursor::Goto {
    cursor::Goto(
        x as u16 * 2 + 1,
        (classic_data::HEIGHT as u16 - 1) - y as u16 + 1,
    )
}

pub struct Render<W: Write>(RawTerminal<cursor::HideCursor<W>>);

impl<W: Write> Render<W> {
    pub fn new(writer: W) -> io::Result<Self> {
        cursor::HideCursor::from(writer).into_raw_mode().map(Self)
    }
}

impl<W: Write> Render<W> {
    fn render_board(&mut self, board: &Board) -> io::Result<()> {
        write!(self.0, "{}{}{}", color::Fg(color::Reset), clear::All, cursor::Goto(1, 1))?;
        self.0.suspend_raw_mode()?;

        let tile_rows = board.tiles().chunks_exact(classic_data::WIDTH as usize);

        for row in tile_rows.rev() {
            for tile in row { self.render_tile(tile)?; }
            writeln!(self.0)?;
        }

        self.0.activate_raw_mode()?;
        Ok(())
    }

    fn render_tile(&mut self, tile: &TileKind) -> io::Result<()> {
        use TileKind::*;
        match tile {
            Empty                  => write!(self.0, "  "),
            Dot(PacDot::Pellet)    => write!(self.0, "{} .", color::Fg(color::White  )),
            Dot(PacDot::Energizer) => write!(self.0, "{} o", color::Fg(color::White  )),
            Wall                   => write!(self.0, "{}##", color::Fg(color::Blue   )),
            GhostDoor              => write!(self.0, "{}--", color::Fg(color::Magenta)),
        }
    }

    fn render_player(&mut self, player: &Player) -> io::Result<()> {
        write!( self.0, "{}{}C", goto_coord(player.entity.coord), color::Fg(color::LightYellow))
    }

    fn render_ghost<Color: color::Color + Copy>(&mut self, ghost: &Ghost, color: Color) -> io::Result<()> {
        write!(self.0, "{}{}", goto_coord(ghost.entity.coord), color::Fg(color),)?;

        use GhostAppearance::*;
        let repr = match ghost.appearance {
            Normal => "M",
            Blue => "W",
            Eyes => "%",
        };
        write!(self.0, "{}", repr)?;

        if let GhostBehavior::Target(Some(target)) = ghost.behavior {
            write!(self.0, "{}{}X", goto_coord(target), color::Fg(color))?;
        }

        Ok(())
    }

    pub fn render(&mut self, game: &Game, _px_offset: u8) -> io::Result<()> {
        self.render_board(&game.board)?;

        self.render_player(&game.player)?;

        self.render_ghost(&game.inky, color::Blue)?;
        self.render_ghost(&game.blinky, color::Red)?;
        self.render_ghost(&game.pinky, color::Magenta)?;
        self.render_ghost(&game.clyde, color::Yellow)?;

        write!(self.0, "{}", color::Fg(color::Reset))?;

        self.0.flush()
    }
}

#[cfg(test)]
pub mod tests {
    #[test]
    fn require_tile_width() {
        assert_eq!(pacperson::game::TILE_PX, 3);
    }
}