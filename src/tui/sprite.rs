#![allow(dead_code)]
use pacperson::game::TILE_PX;

pub type Sprite = [&'static str; TILE_PX as usize];


pub const PLAYER: Sprite = [
    "###",
    "#  ",
    "###",
];

pub const GHOST: Sprite = [
    " # ",
    "###",
    "# #",
];