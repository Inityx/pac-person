use std::io::{self, prelude::*};
use pacperson::{
    num::Direction,
    game::{Game, GameStatus},
};
use termion::{
    event::Key,
    input::TermRead,
};
use crate::render::Render;

enum Action {
    Stick(Direction),
    Advance,
    Quit,
}

fn key_action(key: Key) -> Option<Action> {
    let action = match key {
        Key::Left  => Action::Stick(Direction::Left),
        Key::Right => Action::Stick(Direction::Right),
        Key::Up    => Action::Stick(Direction::Up),
        Key::Down  => Action::Stick(Direction::Down),
        Key::Char('\n') => Action::Advance,
        Key::Char('q')  => Action::Quit,
        _ => return None,
    };

    Some(action)
}

pub fn play<R: Read, W: Write>(input: R, output: W, mut game: Game) -> io::Result<GameStatus> {
    let mut render = Render::new(output)?;

    render.render(&game, 0)?;

    for key in input.keys() {
        let action = match key_action(key?) {
            Some(action) => action,
            None => continue,
        };

        match action {
            Action::Quit => { return Ok(GameStatus::Lose); }
            Action::Stick(direction) => game.input(direction),
            Action::Advance => {
                let px_offset = match game.tick() {
                    GameStatus::Playing { px_offset } => px_offset,
                    state => return Ok(state),
                };
                render.render(&game, px_offset)?;
            }
        }

        eprintln!();
        eprintln!("Frighten: {:?}, Input: {:?}", game.frighten, game.next_input);
        std::io::stderr().flush().unwrap();
    }

    // Raw mode stdin should never EOF
    Err(io::ErrorKind::BrokenPipe.into())
}
