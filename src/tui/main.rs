mod render;
mod sprite;
mod interface;

use pacperson::{Game, GameStatus};

fn main() {
    let outcome = interface::play(
        std::io::stdin().lock(),
        std::io::stdout().lock(),
        Game::new(),
    ).unwrap();

    match outcome {
        GameStatus::Win { score } => println!("You win, {} points", score),
        GameStatus::Lose => println!("You lose"),
        GameStatus::Playing { .. } => unreachable!(),
    }
}