#![no_std]
#![feature(
    const_fn,
    const_if_match,
    trait_alias,
    unboxed_closures,
    fn_traits,
)]

pub mod game;
pub mod num;
pub mod func;

pub use game::{Game, GameStatus};