use super::Entity;
use crate::{
    num::{Direction, TileCoord, TileIndex},
    game::{
        classic_data,
        board::{Board, TileKind},
    },
};

pub struct Player {
    pub entity: Entity,
    pub mouth_open: bool,
}

impl Player {
    pub fn new() -> Self {
        Self {
            entity: Entity::new(classic_data::init_coord::PLAYER),
            mouth_open: true,
        }
    }

    pub fn anim_tick(&mut self) {
        self.mouth_open = !self.mouth_open;
    }

    pub fn navigate(&mut self, board: &Board, direction: Option<Direction>) {
        self.entity.coord = self.entity.next_step;

        if let Some(direction) = direction {
            self.try_turn(board, direction);
        }

        let (next_coord, next_tile_kind) = self.compute_dest_tile(board);

        if next_tile_kind.is_walkable() {
            self.entity.next_step = next_coord;
        }
    }

    fn try_turn(&mut self, board: &Board, direction: Direction) {
        let desired = self.entity.coord + direction.as_coord::<TileIndex>();

        if board[desired].is_walkable() {
            self.entity.direction = direction;
        }
    }

    fn compute_dest_tile(&self, board: &Board) -> (TileCoord, TileKind) {
        let forward_coord = self.entity.coord + self.entity.direction;

        if let Some(&tile) = board.get(forward_coord) {
            return (forward_coord, tile)
        }

        let teleported_coord = Board::teleport(forward_coord);
        let &tile = board.get(teleported_coord).expect("Teleported outside board");
        (teleported_coord, tile)
    }
}
