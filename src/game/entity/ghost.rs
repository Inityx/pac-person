#![allow(unused_variables)]
use super::Entity;
use crate::{
    game::{
        board::{Board, TileKind, EMERGING_TARGET, HOME_TARGET},
        classic_data,
    },
    num::*,
};

pub struct Ghost {
    pub entity: Entity,
    pub appearance: Appearance,
    pub behavior: Behavior,
    pub personality: Personality,
}

impl Ghost {
    pub fn new(personality: Personality) -> Self {
        Self {
            entity: Entity::new(personality.init_pos()),
            appearance: Appearance::Normal,
            behavior: Behavior::Emerge,
            personality,
        }
    }

    pub fn anim_tick(&mut self) {
        // todo
    }

    fn turn_around(&mut self) {
        self.entity.direction.invert();
    }

    pub fn update_target(&mut self, context: AiContext, is_chase: bool) {
        if let Behavior::Target(ref mut target) = self.behavior {
            let new_target = if is_chase {
                self.personality.chase_target(context)
            } else {
                self.personality.scatter_target()
            };

            *target = Some(new_target);
        }
    }

    pub fn auto_state_transition(&mut self, board: &Board, frighten: bool) {
        use Behavior::*;
        use Appearance::*;

        let (new_behavior, new_appearance) = match (self.behavior, self.entity.coord, frighten) {
            (Target(_), _, true) => (Frightened, Blue),
            (Frightened, _, false) |
            (Emerge, EMERGING_TARGET, _) => (Target(None), Normal),
            (Eaten, HOME_TARGET, _) => (Emerge, Normal),
            _ => return,
        };

        self.behavior = new_behavior;
        self.appearance = new_appearance;
    }

    pub fn step(&mut self, board: &Board) {
        // Complete previous step
        self.entity.coord = self.entity.next_step;

        use Behavior::*;
        let new_direction = match self.behavior {
            Target(t)  => self.next_step_targeted(board, t.unwrap()),
            Emerge     => self.next_step_emerging(board),
            Frightened => self.next_step_frightened(),
            Eaten      => self.next_step_eaten(),
        };

        self.entity.next_step = self.entity.coord + new_direction;
        self.entity.direction = new_direction;
    }

    fn next_step_targeted(&self, board: &Board, target: TileCoord) -> Direction {
        DirectionPriority::new()
            .rev() // min_by keeps final element when tied for minimum
            .filter(|&direction| direction != -self.entity.direction)
            .map(|direction| (direction, self.entity.coord + direction))
            .filter(|&(direction, coord)| board
                .get(coord)
                .copied()
                .filter(TileKind::is_walkable)
                .is_some()
            )
            .min_by_key(|(_, coord)| coord.dist_sq_to(target))
            .map(|(direction, _)| direction)
            .unwrap_or_else(|| panic!("No valid next tile for ghost at {}", self.entity.coord))
    }

    fn next_step_emerging(&self, board: &Board) -> Direction {
        DirectionPriority::new()
            .rev() // min_by keeps final element when tied for minimum
            .map(|direction| (direction, self.entity.coord + direction))
            .filter(|&(direction, coord)| board
                .get(coord)
                .copied()
                .filter(TileKind::is_walkable_emerging)
                .is_some()
            )
            .min_by_key(|(_, coord)| coord.dist_sq_to(EMERGING_TARGET))
            .map(|(direction, _)| direction)
            .unwrap_or_else(|| panic!("No valid next tile for ghost at {}", self.entity.coord))
    }

    fn next_step_frightened(&self) -> Direction {
        // No Backward
        // No through walls
        // Random selection
        todo!()
    }

    fn next_step_eaten(&self) -> Direction {
        todo!()
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub struct AiContext {
    pub player: Entity,
    pub blinky: TileCoord,
    pub clyde: TileCoord,
}

pub enum Appearance {
    Normal,
    Blue,
    Eyes,
}

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum Behavior {
    Target(Option<TileCoord>),
    Frightened,
    Emerge,
    Eaten,
}

#[derive(Clone, Copy)]
pub enum Personality { Inky, Blinky, Pinky, Clyde }

impl Personality {
    fn chase_target(&self, context: AiContext) -> TileCoord {
        use Personality::*;

        match self {
            Inky => {
                let tile_in_front = context.player.coord + context.player.direction.as_coord::<TileIndex>() * 2;
                -(context.blinky - tile_in_front) + tile_in_front
            }
            Blinky => context.player.coord,
            Pinky => context.player.coord + context.player.direction.as_coord::<TileIndex>() * 4,
            Clyde => {
                let player_distance_sq = (context.clyde - context.player.coord).magnitude_sq();
                if player_distance_sq > (8 * 8) {
                    context.player.coord
                } else {
                    self.scatter_target()
                }
            }
        }
    }

    #[inline(always)]
    fn scatter_target(&self) -> TileCoord {
        use Personality::*;
        use classic_data::scatter::*;

        match self {
            Inky => INKY,
            Pinky => PINKY,
            Blinky => BLINKY,
            Clyde => CLYDE,
        }
    }

    #[inline(always)]
    fn init_pos(&self) -> TileCoord {
        use Personality::*;
        use classic_data::init_coord::*;

        match self {
            Inky => INKY,
            Pinky => PINKY,
            Blinky => BLINKY,
            Clyde => CLYDE,
        }
    }
}
