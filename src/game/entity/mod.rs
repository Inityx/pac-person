pub mod ghost;
pub mod player;

use crate::num::*;

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Entity {
    pub next_step: TileCoord,
    pub coord: TileCoord,
    pub direction: Direction,
}

impl Entity {
    pub fn new(coord: TileCoord) -> Self {
        Self {
            coord,
            direction: Direction::Up,
            next_step: coord,
        }
    }
}

