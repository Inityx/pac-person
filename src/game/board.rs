use core::{
    iter::FromIterator,
    ops::{Index, IndexMut},
};

use crate::{
    game::classic_data,
    num::*,
};

pub use classic_data::scatter::{
    EMERGING as EMERGING_TARGET,
    HOME as HOME_TARGET,
};

pub struct Board {
    tiles: [TileKind; classic_data::TILE_COUNT],
}

impl Board {
    pub fn new() -> Self {
        classic_data::TILE_LAYOUT.chars().collect()
    }

    const fn empty() -> Self {
        Self {
            tiles: [TileKind::Empty; classic_data::TILE_COUNT],
        }
    }

    pub fn teleport(Coord { x, y }: TileCoord) -> TileCoord {
        if y.is_negative() { return Coord { x, y: classic_data::HEIGHT - 1 }; }
        if x.is_negative() { return Coord { x: classic_data::WIDTH - 1, y }; }
        if x >= classic_data::WIDTH  { return Coord { x: 0, y }; }
        if y >= classic_data::HEIGHT { return Coord { x, y: 0 }; }

        panic!("Teleport called with an interior coordinate");
    }

    fn contains_index(coord: TileCoord) -> bool {
        coord.x >= 0 && coord.y >= 0 && coord.x < classic_data::WIDTH && coord.y < classic_data::HEIGHT
    }

    const fn coord_to_tile(coord: TileCoord) -> usize {
        coord.y as usize * classic_data::WIDTH as usize + coord.x as usize
    }

    pub fn get(&self, coord: TileCoord) -> Option<&TileKind> {
        if !Self::contains_index(coord) { return None; }

        let index = Self::coord_to_tile(coord);
        Some(unsafe { self.tiles.get_unchecked(index) })
    }

    pub fn get_mut(&mut self, coord: TileCoord) -> Option<&mut TileKind> {
        if !Self::contains_index(coord) { return None; }

        let index = Self::coord_to_tile(coord);
        Some(unsafe { self.tiles.get_unchecked_mut(index) })
    }

    pub fn tiles(&self) -> &[TileKind] {
        &self.tiles
    }
}

impl Index<TileCoord> for Board {
    type Output = TileKind;
    fn index(&self, index: TileCoord) -> &Self::Output {
        self.get(index).expect("index {} out of range for board")
    }
}

impl IndexMut<TileCoord> for Board {
    fn index_mut(&mut self, index: TileCoord) -> &mut Self::Output {
        self.get_mut(index).expect("index {} out of range for board")
    }
}

impl<T: Into<TileKind>> FromIterator<T> for Board {
    fn from_iter<I: IntoIterator<Item=T>>(iter: I) -> Self {
        let mut board = Self::empty();
        let tiles = iter
            .into_iter()
            .map(Into::into)
            .take(classic_data::TILE_COUNT);

        for (i, tile) in tiles.enumerate() {
            board.tiles[i] = tile;
        }

        board
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum PacDot {
    Pellet,
    Energizer,
}

impl PacDot {
    pub fn points(&self) -> Score {
        match self {
            PacDot::Pellet    => classic_data::points::PELLET,
            PacDot::Energizer => classic_data::points::ENERGIZER,
        }
    }
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum TileKind {
    Empty,
    Dot(PacDot),
    Wall,
    GhostDoor,
}

impl TileKind {
    pub fn is_dot(&self) -> bool {
        matches!(self, Self::Dot(_))
    }

    pub fn is_walkable(&self) -> bool {
        use TileKind::*;
        match self {
            Wall | GhostDoor => false,
            Empty | Dot(_) => true,
        }
    }
    pub fn is_walkable_emerging(&self) -> bool {
        use TileKind::*;
        match self {
            Wall => false,
            Empty | Dot(_) | GhostDoor => true,
        }
    }

    pub fn take(&mut self) -> Option<PacDot> {
        use TileKind::*;
        let dot = match *self {
            Empty => return None,
            Wall | GhostDoor => panic!("Tried to consume a wall"),
            Dot(dot) => dot,
        };

        *self = TileKind::Empty;
        Some(dot)
    }
}

impl From<char> for TileKind {
    fn from(c: char) -> Self {
        match c {
            ' ' => Self::Empty,
            '.' => Self::Dot(PacDot::Pellet),
            'o' => Self::Dot(PacDot::Energizer),
            '#' => Self::Wall,
            '-' => Self::GhostDoor,
            _ => panic!("Invalid Tile character: {:?}", c),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn board_from_string() {
        let board: Board = " .o#-".chars().collect();

        use TileKind::*;
        use PacDot::*;
        assert_eq!(board[Coord::at(0, 0)], Empty         );
        assert_eq!(board[Coord::at(1, 0)], Dot(Pellet)   );
        assert_eq!(board[Coord::at(2, 0)], Dot(Energizer));
        assert_eq!(board[Coord::at(3, 0)], Wall          );
        assert_eq!(board[Coord::at(4, 0)], GhostDoor     );
        assert_eq!(board[Coord::at(5, 0)], Empty         );
    }
}
