#![allow(dead_code)]
pub mod board;
pub mod entity;
pub mod classic_data;

use crate::{
    num::{Score, Direction, Periodic, Tick, CountDown, BoolSequence},
    func::Not,
};
use entity::{
    player::Player,
    ghost::{Ghost, Personality, AiContext as GhostAiContext},
};
use board::{Board, TileKind, PacDot};

pub const TILE_PX: u8 = 3;
use classic_data::FRIGHTEN_LENGTH;
use classic_data::CHASE_SEQUENCE;

pub enum GameStatus {
    Win { score: u16 },
    Lose,
    Playing { px_offset: u8 },
}

pub struct Game {
    pub board: Board,
    pub score: Score,

    pub next_input: Option<Direction>,
    pub ticks: Periodic,
    pub chase_sequence: BoolSequence<'static>,
    pub frighten: CountDown,

    pub player: Player,
    pub inky: Ghost,
    pub pinky: Ghost,
    pub blinky: Ghost,
    pub clyde: Ghost,
}

impl Game {
    pub fn new() -> Self {
        use Personality::*;
        Self {
            board: board::Board::new(),
            score: 0,

            next_input: None,
            ticks: Periodic::once_every(TILE_PX),
            chase_sequence: BoolSequence::starting_with(false, &CHASE_SEQUENCE),
            frighten: CountDown::immediate(),

            player: Player::new(),
            inky:   Ghost::new(Inky),
            pinky:  Ghost::new(Pinky),
            blinky: Ghost::new(Blinky),
            clyde:  Ghost::new(Clyde),
        }
    }

    pub fn input(&mut self, input: Direction) {
        self.next_input = Some(input);
    }

    /// Returns game status and number of pixels by which the renderer should offset sprites
    pub fn tick(&mut self) -> GameStatus {
        self.anim_tick();

        match self.ticks.next() {
            Tick::Tock => {
                self.logic_tick();

                if self.lose_condition() {
                    GameStatus::Lose
                } else if self.win_condition() {
                    GameStatus::Win { score: self.score }
                } else {
                    GameStatus::Playing { px_offset: 0 }
                }
            }
            Tick::Tick(elapsed) => GameStatus::Playing { px_offset: elapsed.get() },
        }
    }

    fn anim_tick(&mut self) {
        self.player.anim_tick();

        self.inky  .anim_tick();
        self.pinky .anim_tick();
        self.blinky.anim_tick();
        self.clyde .anim_tick();
    }

    fn logic_tick(&mut self) {
        let is_chase = self.chase_sequence.next();
        let frighten = self.frighten.next().is_some();

        self.player.navigate(&self.board, self.next_input.take());

        let ai_context = GhostAiContext {
            player: self.player.entity,
            blinky: self.blinky.entity.coord,
            clyde: self.clyde.entity.coord,
        };

        // Trying to DRY this in no_std is an ownership nightmare, maybe do a macro later
        // Ghost AI
        self.inky  .update_target(ai_context, is_chase);
        self.pinky .update_target(ai_context, is_chase);
        self.blinky.update_target(ai_context, is_chase);
        self.clyde .update_target(ai_context, is_chase);

        self.inky  .step(&self.board);
        self.pinky .step(&self.board);
        self.blinky.step(&self.board);
        self.clyde .step(&self.board);

        // Pickups
        if let Some(dot) = self.board[self.player.entity.coord].take() {
            self.score += dot.points();
            if dot == PacDot::Energizer {
                self.frighten = CountDown::from(FRIGHTEN_LENGTH);
            }
        }

        self.inky  .auto_state_transition(&self.board, frighten);
        self.pinky .auto_state_transition(&self.board, frighten);
        self.blinky.auto_state_transition(&self.board, frighten);
        self.clyde .auto_state_transition(&self.board, frighten);
    }

    fn lose_condition(&self) -> bool {
        let player = self.player.entity.coord;
        let ghosts = [
            self.inky  .entity.coord,
            self.pinky .entity.coord,
            self.blinky.entity.coord,
            self.clyde .entity.coord,
        ];
        ghosts.iter().any(|&ghost| ghost == player)
    }

    fn win_condition(&self) -> bool {
        self.board.tiles().iter().all(Not(TileKind::is_dot))
    }
}
