use crate::num::*;

pub const WIDTH: TileIndex = 19;
pub const HEIGHT: TileIndex = 22;
pub const TILE_COUNT: usize = WIDTH as usize * HEIGHT as usize;
pub const FRIGHTEN_LENGTH: u8 = 64;
pub const CHASE_SEQUENCE: [u8;0] = [];

// Board layouts are upside down because text advances
// downward and the Y coord advances upward
pub const TILE_LAYOUT: &str = concat!(
    "###################",
    "#.................#",
    "#.######.#.######.#",
    "#....#...#...#....#",
    "##.#.#.#####.#.#.##",
    "#o.#..... .....#.o#",
    "#.##.###.#.###.##.#",
    "#........#........#",
    "####.# ##### #.####",
    "   #.#        .#   ",
    "####.# ##### #.####",
    "    .  #   #  .    ",
    "####.# ##-## #.####",
    "   #.#        .#   ",
    "####.### # ###.####",
    "#....#...#...#....#",
    "#.##.#.#####.#.##.#",
    "#.................#",
    "#.##.###.#.###.##.#",
    "#o##.###.#.###.##.#",
    "#........#........#",
    "###################",
);

pub mod points {
    use super::*;

    pub const PELLET: Score = 10;
    pub const ENERGIZER: Score = 50;
}

pub mod init_coord {
    use super::*;

    pub const PLAYER: TileCoord = Coord::at(9,5);

    pub const INKY:   TileCoord = Coord::at(8, 11);
    pub const PINKY:  TileCoord = Coord::at(9, 11);
    pub const BLINKY: TileCoord = Coord::at(9, 13);
    pub const CLYDE:  TileCoord = Coord::at(10, 11);
}

pub mod scatter {
    use super::*;

    pub const INKY:   TileCoord = Coord::at(18, 0);
    pub const PINKY:  TileCoord = Coord::at(0, 21);
    pub const BLINKY: TileCoord = Coord::at(18, 21);
    pub const CLYDE:  TileCoord = Coord::at(0, 0);

    pub const EMERGING: TileCoord = Coord::at(9, 13);
    pub const HOME: TileCoord = Coord::at(9, 11);
}
